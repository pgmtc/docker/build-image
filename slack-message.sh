#!/bin/bash
SLACK_MESSAGE="$1"

if [ -z "$SLACK_NOTIFY_URL" ]
then
    echo "WARN: SLACK_NOTIFY_URL not defined, not going to send any message"
    exit 0
else
    curl -X POST -H 'Content-type: application/json' --data "{\"text\":\"${SLACK_MESSAGE}\"}" ${SLACK_NOTIFY_URL}
fi



